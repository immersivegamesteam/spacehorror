using System;
using SenneEntreteiner.Player;
using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.XR;

namespace SenneEntreteiner.Selectable
{
    public class PickUp2Hands : MonoBehaviour, IInteract
    {
        public ScriptableCollectableItem collectableItemData;

        public void Interact(PlayerCollectiblesManager collectiblesManager)
        {
            Debug.Log("O Jogador interagiu com o objeto" + collectableItemData.name);
            var handTransform = collectiblesManager.PlayerHands;
            Rigidbody rb;
            if (collectiblesManager.ItemInHand != null)
            {
                Debug.Log("ENTROU????");
                var oldChildren = handTransform.GetChild(0);
                rb = oldChildren.GetComponent<Rigidbody>();
                if (rb != null) rb.isKinematic = false;
                oldChildren.parent = null;
                var transform1 = this.transform;
                oldChildren.transform.SetPositionAndRotation(transform1.position, transform1.rotation);
                oldChildren.gameObject.layer = collectiblesManager.DefaultLayer;
            }
            collectiblesManager.AddToHand(collectableItemData);
            rb = GetComponent<Rigidbody>();
            if (rb != null) rb.isKinematic = true;
            transform.parent = handTransform;
            transform.SetSiblingIndex(0);
            transform.SetPositionAndRotation(handTransform.position,handTransform.rotation);
            gameObject.layer = collectiblesManager.InHandLayer;
        }
        

        public void EnableObject()
        {
            enabled = true;
        }

        public void DisableObject()
        {
            enabled = false;
        }
    }
}
