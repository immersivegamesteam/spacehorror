using SenneEntreteiner.Player;

namespace SenneEntreteiner.Selectable
{
    public interface IInteract
    {
        public void Interact(PlayerCollectiblesManager CollectiblesManager);
        public void EnableObject();
        public void DisableObject();
    }
}
