using UnityEngine;

namespace SenneEntreteiner.Selectable
{
    public interface ISelectionResponse 
    {
        void OnSelect(Transform selection);
        void OnDeselect(Transform selection);
    }
}
