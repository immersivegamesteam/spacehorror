using UnityEngine;

namespace SenneEntreteiner.Selectable
{
    public interface IRayProvider
    {
        Ray CreateRay();
    }
}
