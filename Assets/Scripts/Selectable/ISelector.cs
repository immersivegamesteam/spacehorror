using UnityEngine;

namespace SenneEntreteiner.Selectable
{
    public interface ISelector
    {
        void Check(Ray ray);
        Transform GetSelection();
    }
}
