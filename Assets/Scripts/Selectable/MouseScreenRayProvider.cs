using UnityEngine;
using UnityEngine.InputSystem;

namespace SenneEntreteiner.Selectable
{
    public class MouseScreenRayProvider : MonoBehaviour, IRayProvider
    {
       public Ray CreateRay()
       {
           return Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue());
       }
    }
}
