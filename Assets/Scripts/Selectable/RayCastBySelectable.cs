using UnityEngine;

namespace SenneEntreteiner.Selectable
{
    public class RayCastBySelectable : MonoBehaviour, ISelector
    {
        private Transform _selection;
        [SerializeField] private int maxDistance;
        [SerializeField] private LayerMask layerMask = default;
        private IInteract _interact;
        public void Check(Ray ray)
        {
            _selection = null;
            if (!Physics.Raycast(ray, out var hit, maxDistance, layerMask)) return;
        
            var selection = hit.transform;
            _interact = selection.GetComponent<IInteract>();
            if (_interact != null) _selection = selection;
        }

        public Transform GetSelection()
        {
            return _selection;
        }
        
    }
}
