using UnityEngine;
using UnityEngine.Serialization;

namespace SenneEntreteiner.Selectable
{
    public class RayCastByTagSelector : MonoBehaviour, ISelector
    {
        
        [SerializeField] private string selectableTag = "Selectable";
        private Transform _selection;
        [SerializeField] private int maxDistance;
        [SerializeField] private LayerMask LayerMask;

        public void Check(Ray ray)
        {
            _selection = null;

            if (!Physics.Raycast(ray, out var hit, maxDistance,LayerMask)) return;
        
            var selection = hit.transform;
            if (selection.CompareTag(selectableTag))
            {
                _selection = selection;
            }
        }

        public Transform GetSelection()
        {
            return _selection;
        }
    }
}
