using SenneEntreteiner.Player;
using UnityEngine;
using UnityEngine.Serialization;

namespace SenneEntreteiner.Selectable
{
    public class PickUp2Inventory : MonoBehaviour, IInteract
    {
        [SerializeField] private bool isDroppable;
        public ScriptableCollectableItem collectableItemData;

        public void Interact(PlayerCollectiblesManager collectiblesManager)
        {
            var playerInventory = collectiblesManager.scriptablePlayerInventory;
            if (playerInventory.IsUniqueItem(collectableItemData)) return;
            playerInventory.AddItem(collectableItemData);
            Debug.Log("O Jogador interagiu com o objeto" + collectableItemData.name);
            this.DisableObject();
        }

        public void EnableObject()
        {
            this.gameObject.SetActive(true);
        }

        public void DisableObject()
        {
            this.gameObject.SetActive(false);
        }
    }
}
