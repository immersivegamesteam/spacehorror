﻿using UnityEngine;

namespace SenneEntreteiner.Selectable
{
    [CreateAssetMenu(fileName = "ItemData", menuName = "ScriptableObjects/CollectableItem", order = 1)]
    public class ScriptableCollectableItem: ScriptableObject
    {
        public new string name;
        public Transform idealTransform;
    }
}