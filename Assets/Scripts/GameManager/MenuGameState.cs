using UnityEngine;

namespace SenneEntreteiner
{
    public class MenuGameState : GameBaseState
    {
        private Transform _uiMenu;
        private Transform _camMenu;
        public MenuGameState(GameStateManager gameStateManager) : base(gameStateManager)
        {
            _uiMenu = gameStateManager.gameObjectUIMenu;
            _camMenu = gameStateManager.gameObjetUiMenuCam;
        }
        
        public override void EnterState()
        {
            if (!_camMenu || !_uiMenu) return;
            Debug.Log("Entrou No Menu");
            _camMenu.gameObject.SetActive(true);
            _uiMenu.gameObject.SetActive(true);
        }

        public override void UpdateState()
        {
            
        }

        public override void ExitState()
        {
            if (!_camMenu || !_uiMenu) return;
            Debug.Log("Saiu do Menu");
            _camMenu.gameObject.SetActive(false);
            _uiMenu.gameObject.SetActive(false);
        }
    }
}
