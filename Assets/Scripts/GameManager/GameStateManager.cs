using System.Collections.Generic;
using UnityEngine;

namespace SenneEntreteiner
{
    public class GameStateManager : MonoBehaviour
    {
        [SerializeField] public Transform gameObjectPlayer;
        [SerializeField] public Transform gameObjectPlayerCam;
        [SerializeField] public Transform gameObjectUIMenu;
        [SerializeField] public Transform gameObjetUiMenuCam;
        public GameBaseState CurrentState { get; private set; }

        private Dictionary<string, GameBaseState> _allStates = new Dictionary<string, GameBaseState>();

        private void Awake()
        {
            _allStates.Add("pause",new PauseGameState(this));
            _allStates.Add("intro",new IntroGameState(this));
            _allStates.Add("menu",new MenuGameState(this));
            _allStates.Add("play",new PlayGameState(this));
            _allStates.Add("interact",new ReadGameState(this));
        }


        // Start is called before the first frame update
        void Start()
        {
            CurrentState = _allStates["menu"];
            CurrentState.EnterState();
        }

        // Update is called once per frame
        void Update()
        {
            CurrentState.UpdateState();
        }

        private void ChangeState(GameBaseState state)
        {
            if (state == CurrentState) return;
            CurrentState.ExitState();
            CurrentState = state;
            CurrentState.EnterState();
        }

        public void OnButtonClickChange(string stateName)
        {
            ChangeState(_allStates[stateName]);
        }
        
    }
}
