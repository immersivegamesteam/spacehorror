using UnityEngine;

namespace SenneEntreteiner
{
    public class PlayGameState : GameBaseState
    {
        private Transform _playerCam;
        private Transform _player;
        public PlayGameState(GameStateManager gameStateManager) : base(gameStateManager)
        {
            _player = gameStateManager.gameObjectPlayer;
            _playerCam = gameStateManager.gameObjectPlayerCam;
        }
        
        public override void EnterState()
        {
            if (!_player || !_playerCam) return;
            Debug.Log("Entrou No Game");
            _player.gameObject.SetActive(true);
            _playerCam.gameObject.SetActive(true);
        }

        public override void UpdateState()
        {
            
        }

        public override void ExitState()
        {
            if (!_player || !_playerCam) return;
            Debug.Log("Entrou do Game");
            _player.gameObject.SetActive(false);
            _playerCam.gameObject.SetActive(false);
        }
    }
}
