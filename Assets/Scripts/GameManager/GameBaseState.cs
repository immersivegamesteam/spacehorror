namespace SenneEntreteiner
{
    public abstract class GameBaseState
    {
        protected GameStateManager GameStateManager;

        protected GameBaseState(GameStateManager gameStateManager)
        {
            GameStateManager = gameStateManager;
        }

        public abstract void EnterState();
        public abstract void UpdateState();
        public abstract void ExitState();
    }
}
