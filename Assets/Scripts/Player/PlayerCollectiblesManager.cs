﻿using SenneEntreteiner.Selectable;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

namespace SenneEntreteiner.Player
{
    [RequireComponent(typeof(IRayProvider))]
    [RequireComponent(typeof(ISelector))]
    public class PlayerCollectiblesManager : MonoBehaviour
    { 
        public ScriptablePlayerInventory scriptablePlayerInventory;
        [SerializeField]
        private Transform playerHands;
        public Transform PlayerHands => playerHands;
        [SerializeField]
        private ScriptableCollectableItem itemInHand;
        public ScriptableCollectableItem ItemInHand => itemInHand;
        private IRayProvider _rayProvider;
        private ISelector _selector;

        private Transform _currentSelection;
        private StarterAssets _inputAssets;
        [SerializeField] private float forceSpeed;
        [SerializeField] protected int inHandLayer;
        public int InHandLayer => inHandLayer;

        [SerializeField] protected int defaultLayer;
        public int DefaultLayer => defaultLayer;

        private void Awake()
        {
            _rayProvider = GetComponent<IRayProvider>();
            _selector = GetComponent<ISelector>();
            _inputAssets = new StarterAssets();
        }

        private void OnEnable()
        {
            _inputAssets.Player.Interact.performed += PlayerInteract;
            _inputAssets.Player.Drop.performed += PlayerDrop;
            _inputAssets.Player.Interact.Enable();
            _inputAssets.Player.Drop.Enable();
        }

        public void Update()
        {
            _selector.Check(_rayProvider.CreateRay());
            _currentSelection = _selector.GetSelection();
        }

        private void PlayerInteract(InputAction.CallbackContext context)
        {
            if (!context.performed || _currentSelection == null) return;
            var objInteract = _currentSelection.GetComponent<IInteract>();
            objInteract.Interact(this);
        }

        private void PlayerDrop(InputAction.CallbackContext context)
        {
            if (!context.performed || itemInHand == null) return;
            itemInHand = null;
            var item = playerHands.GetChild(0);
            var rb = item.GetComponent<Rigidbody>();
            item.gameObject.layer = DefaultLayer;
            item.parent = null;
            
            if (rb == null) return;
            rb.isKinematic = false;
            rb.AddForce(-Vector3.forward * forceSpeed);
        }

        public void AddToHand(ScriptableCollectableItem handedItem)
        {
            itemInHand = handedItem;
        }
        
        private void OnDisable()
        {
            _inputAssets.Player.Interact.performed -= PlayerInteract;
            _inputAssets.Player.Interact.Disable();
            _inputAssets.Player.Drop.performed -= PlayerDrop;
            _inputAssets.Player.Drop.Disable();
        }
    }
}