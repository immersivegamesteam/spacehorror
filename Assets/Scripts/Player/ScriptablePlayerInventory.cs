﻿using System.Collections.Generic;
using SenneEntreteiner.Selectable;
using UnityEngine;

namespace SenneEntreteiner.Player
{
    [CreateAssetMenu(fileName = "Inventory", menuName = "ScriptableObjects/PlayerInventory", order = 2)]
    public class ScriptablePlayerInventory : ScriptableObject
    {
        public List<ScriptableCollectableItem> collectableItems = new List<ScriptableCollectableItem>();

        public void AddItem(ScriptableCollectableItem collectableItem)
        {
            collectableItems.Add(collectableItem);
            Debug.Log("Item adicionado "+ collectableItem.name);
        }

        public void RemoveItem(ScriptableCollectableItem collectableItem)
        {
            collectableItems.Remove(collectableItem);
        }

        public void ClearInventory()
        {
            collectableItems.Clear();
        }

        public bool IsUniqueItem(ScriptableCollectableItem collectableItem)
        {
            return collectableItems.Contains(collectableItem);
        }
    }
}