using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace SenneEntreteiner.UI
{
    public class SpaceHorrorInitialMenu : EditorWindow
    {
        [MenuItem("Window/UI Toolkit/SpaceHorrorInitialMenu")]
        public static void ShowExample()
        {
            SpaceHorrorInitialMenu wnd = GetWindow<SpaceHorrorInitialMenu>();
            wnd.titleContent = new GUIContent("SpaceHorrorInitialMenu");
        }

        public void CreateGUI()
        {
            // Each editor window contains a root VisualElement object
            VisualElement root = rootVisualElement;

            // VisualElements objects can contain other VisualElement following a tree hierarchy.
            VisualElement label = new Label("Hello World! From C#");
            root.Add(label);

            // Import UXML
            var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/UI/SpaceHorrorInitialMenu.uxml");
            VisualElement labelFromUXML = visualTree.Instantiate();
            root.Add(labelFromUXML);

            // A stylesheet can be added to a VisualElement.
            // The style will be applied to the VisualElement and all of its children.
            var styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/UI/SpaceHorrorInitialMenu.uss");
            VisualElement labelWithStyle = new Label("Hello World! With Style");
            labelWithStyle.styleSheets.Add(styleSheet);
            root.Add(labelWithStyle);
        }
    }
}